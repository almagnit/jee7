package jeesimple.data;

import jeesimple.model.Person;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
public class PersonRepository {

    @Inject
    private EntityManager em;

    public Person findById(Long id) {
        return em.find(Person.class, id);
    }

    public void create(Person person) {
        em.persist(person);
    }

    public void save(Person person) {
        em.merge(person);
    }

    public void remove(Person person){
        em.remove(person);
    }

    public void remove(Long id){
        em.remove(em.find(Person.class, id));
    }

    public List<Person> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Person> criteria = cb.createQuery(Person.class);
        Root<Person> person = criteria.from(Person.class);
        return em.createQuery(criteria.select(person)).getResultList();
    }
}