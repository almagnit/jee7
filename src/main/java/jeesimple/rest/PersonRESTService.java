package jeesimple.rest;

import jeesimple.data.PersonRepository;
import jeesimple.model.Person;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@RequestScoped
@Path("/persons")
public class PersonRESTService {

    private final static String APPLICATION_JSON = MediaType.APPLICATION_JSON + "; charset=UTF-8";

    @Inject
    private PersonRepository repository;

    @GET
    @Produces(APPLICATION_JSON)
    public List<Person> listAllPersons() {
        return repository.findAll();
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(APPLICATION_JSON)
    public Person getPerson(@PathParam("id") Long id) {
        return repository.findById(id);
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Person createPerson(Person person) {
        repository.create(person);
        return person;
    }

    @PUT
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response savePerson(Person person) {
        repository.save(person);
        return Response.ok().build();
    }

    @DELETE
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response deletePerson(Person person) {
        repository.remove(person);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response deletePerson(@PathParam("id") Long id) {
        repository.remove(id);
        return Response.ok().build();
    }

}
