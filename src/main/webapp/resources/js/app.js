loadPersonList = function () {
    var template = $.templates("#personItem");

    $.ajax({
        url: "rest/persons"
    }).then(function (data) {
        var htmlOutput = template.render(data);
        $("#personList").html(htmlOutput);
    });
};

initDatePicker = function () {
    $('#birthday').datepicker({
        language: 'ru',
        format: 'dd.mm.yyyy',
        toggleActive: true
    });
};

initFormControls = function () {
    $("#create").click(function () {
        var person = {
            name: $("#name").val(),
            middlename: $("#middlename").val(),
            lastname: $("#lastname").val(),
            birthday: $("#birthday").val()
        };
        $.ajax({
            type: "POST",
            url: "rest/persons",
            dataType: "text",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify(person)
        }).done(
            function (data) {
                var person = JSON.parse(data);
                $("#id").val(person.id);
                loadPersonList();
            }
        ).fail(
            function (msg, status) {
                alert(status);
            }
        );
    });
    $("#update").click(function () {
        var person = {
            id: $("#id").val(),
            name: $("#name").val(),
            middlename: $("#middlename").val(),
            lastname: $("#lastname").val(),
            birthday: $("#birthday").val()
        };
        $.ajax({
            type: "PUT",
            url: "rest/persons",
            dataType: "text",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify(person)
        }).done(
            function () {
                loadPersonList();
            }
        ).fail(
            function (msg, status) {
                alert(status);
            }
        );
    });
    $("#delete").click(function () {
        $.ajax({
            type: "DELETE",
            url: "rest/persons/" + $("#id").val(),
            dataType: "text",
            contentType: "application/json; charset=UTF-8"
        }).done(
            function () {
                $("#id").val("");
                $("#name").val("");
                $("#middlename").val("");
                $("#lastname").val("");
                $("#birthday").val("");
                loadPersonList();
            }
        ).fail(
            function (msg, status) {
                alert(status);
            }
        );
    });

};

getPerson = function (itemId) {
    $.ajax({
        url: "rest/persons/" + itemId,
        success: function (data) {
            $("#id").val(data.id);
            $("#name").val(data.name);
            $("#middlename").val(data.middlename);
            $("#lastname").val(data.lastname);
            $("#birthday").val(data.birthday);
            console.log(data);
        }
    });
};

$(document).ready(function () {
    loadPersonList();
    initDatePicker();
    initFormControls();
});